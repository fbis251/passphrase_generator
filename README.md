# Passphrase Generator

Generates a passphrase of random English words.

## Dictionary Source

The english words are sourced from the english-words project's [words_alpha.txt](https://github.com/dwyl/english-words) and
the 10,000 most common English words in order of frequence project from [Google's Trillion Word Corpus](https://github.com/first20hours/google-10000-english)

## Passphrase Generation

The generator uses Go's `crypto/rand` package to get a uniform random integer between 0 and (n - 1), where n is the total
number of words in the dictionary.
