package main

import (
	"bytes"
	"crypto/rand"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"os"
	"strings"
)

const (
	// The file to read the words from, one word per line
	defaultWordsFile = "words.txt"
	// The number of words to use in the passphrase
	defaultPassphraseLength = 5

	exitCodeFileReadError = 1
	exitCodePassGenError  = 2
)

func main() {
	printNewline := flag.Bool("n", false, "Print newline at the end of the passphrase")
	wordsFile := flag.String("f", defaultWordsFile, "The file to read the words from, one word per line")
	passphraseLength := flag.Int("l", defaultPassphraseLength, "The number of words to use in the passphrase")
	flag.Parse()
	l := log.New(os.Stderr, "", 0)

	words, err := readWords(*wordsFile)
	if err != nil {
		l.Printf("Error reading file: %s\n", err)
		os.Exit(exitCodeFileReadError)
	}

	passphrase, err := generatePassphrase(words, *passphraseLength)
	if err != nil {
		l.Printf("Error generating passphrase: %s\n", err)
		os.Exit(exitCodePassGenError)
	}
	if *printNewline {
		fmt.Println(passphrase)
	} else {
		fmt.Print(passphrase)
	}
}

func generatePassphrase(words []string, passphraseLength int) (string, error) {
	totalWords := int64(len(words))
	passphraseBuffer := bytes.NewBufferString("")

	for i := 0; i < passphraseLength; i++ {
		// Get a uniformly random int between 0 and the total number of words
		randomIndex, err := getRandomInt(totalWords)
		if err != nil {
			return "", err
		}
		// Use the random index to get a word from the words array
		currentWord := strings.TrimSpace(words[randomIndex])
		if i != 0 {
			// Only prefix a space if this isn't the first word to prevent a space at the beginning of the phrase
			if _, err := passphraseBuffer.WriteString(" "); err != nil {
				return "", err
			}
		}
		if _, err := passphraseBuffer.WriteString(currentWord); err != nil {
			return "", err
		}
	}
	return passphraseBuffer.String(), nil
}

func getRandomInt(maxNumber int64) (int64, error) {
	wordCount := big.NewInt(maxNumber)
	randomInt, err := rand.Int(rand.Reader, wordCount)
	if err != nil {
		return 0, err
	}
	return randomInt.Int64(), nil
}

func readWords(filename string) ([]string, error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	lines := strings.Split(string(content), "\n")
	return lines, nil
}
